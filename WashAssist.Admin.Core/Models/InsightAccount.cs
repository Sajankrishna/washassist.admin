﻿using WashAssist.Admin.Core.Attributes;
using System;

namespace WashAssist.Admin.Models
{
    [Table(Name = "Accounts")]
    public class InsightAccounts
    {
        [Identifier]
        public long AccountID { get; set; }
        public string DatabaseName { get; set; }
        public string ServerIp { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string InsightCustomerCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsWALive { get; set; }
        public string SubDomain { get; set; }
        public string ExternalLoginUrl { get; set; }
        public string EmailAddress { get; set; }
        public string EmailProviderKey { get; set; }
        public string SMSProviderKey { get; set; }
        public string SMSProviderNumber { get; set; }
        public string FromEmail { get; set; }
        public string EmailDomainName { get; set; }
        public string SMSProviderTocken { get; set; }
        public Nullable<bool> isPaymentLive { get; set; }
        public string paymentMode { get; set; }
        public string SMSOTPMessage { get; set; }
        public Nullable<bool> bPortalRefundEnabled { get; set; }
        public Nullable<bool> bSurchargeFirstMonthEnabled { get; set; }
    }
}

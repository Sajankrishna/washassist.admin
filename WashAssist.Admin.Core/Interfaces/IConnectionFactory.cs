﻿using System.Data;
using System.Threading.Tasks;

namespace WashAssist.Admin.Core.Interfaces
{
    public interface IConnectionFactory
    {
        //IDbConnection GetConnection(string customerCode);
        string GetConnectionString(string customerCode);
    }
}

﻿using Dapper;
using System.Data;
using System.Threading.Tasks;

namespace WashAssist.Admin.Core.Interfaces
{
    public interface IDapperConnectionWrapper
    {
        Task<T> QueryFirstOrDefaultAsyncOverride<T>(IDbConnection connection, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
    }
}
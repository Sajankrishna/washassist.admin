﻿// ***********************************************************************
// Assembly         : WashAssist.Admin.Core
// Author           : Sajeev SL
// Created          : 06-28-2019
//
// Last Modified By : Sajeev SL
// Last Modified On : 02-24-2021
// ***********************************************************************
// <copyright file="IRepository.cs" company="WashAssist.Admin.Core">
//     Copyright (c) Micrologic Associates Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WashAssist.Admin.Core.Interfaces
{
    /// <summary>
    /// Interface IRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> : IDisposable  where T : class
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance has identity insert.
        /// </summary>
        /// <value><c>true</c> if this instance has identity insert; otherwise, <c>false</c>.</value>
        bool HasIdentityInsert { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has table identity.
        /// </summary>
        /// <value><c>true</c> if this instance has table identity; otherwise, <c>false</c>.</value>
        bool HasTableIdentity { get; set; }

        #region "Sync Calls"

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        T Create(T entity, string customerCode = null);
        /// <summary>
        /// Bulks the import.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool BulkImport(List<T> entities, string customerCode = null);
        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        int Update(T entity, string customerCode = null);
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        int Delete(int id, string customerCode = null);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        int Delete(Guid id, string customerCode = null);

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        int BulkDelete(IEnumerable<int> ids, string customerCode = null);
        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        int BulkDelete(IEnumerable<long> ids, string customerCode = null);
        int BulkDeleteWithSp(IEnumerable<long> ids, string customerCode = null);

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        int BulkDelete(IEnumerable<Guid> ids, string customerCode = null);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        int Delete(long id, string customerCode = null);
        /// <summary>
        /// Softs the delete.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        int SoftDelete(int id, string customerCode = null);
        /// <summary>
        /// Softs the delete.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>

        int SoftDelete(Guid id, string customerCode = null);
        /// <summary>
        /// Anies the specified predicat expression.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool Any(Expression<Func<T, bool>> predicatExpression, string customerCode = null);
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        IEnumerable<T> GetAll(string customerCode = null);
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        T GetById(int id, string customerCode = null);
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        T GetById(long id, string customerCode = null);
        /// <summary>
        /// Firsts the or default.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        T FirstOrDefault(Expression<Func<T, bool>> predicatExpression, string customerCode = null);
        /// <summary>
        /// Finds the specified predicat expression.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        IEnumerable<T> Find(Expression<Func<T, bool>> predicatExpression, string customerCode = null);

        IEnumerable<T> FindTop(int recordCount, Expression<Func<T, bool>> predicatExpression, string orderBy, bool orderByDesc = true, string customerCode = null);
        #endregion

        #region "Async Calls"

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        Task<T> CreateAsync(T entity, string customerCode = null);
        /// <summary>
        /// Bulks the import asynchronous.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task.</returns>
        Task BulkImportAsync(List<T> entities, string customerCode = null);
        /// <summary>
        /// Updates the asynchronous.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> UpdateAsync(T entity, string customerCode = null);
        /// <summary>
        /// Deletes the asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> DeleteAsync(int id, string customerCode = null);
        /// <summary>
        /// Deletes the asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> DeleteAsync(long id, string customerCode = null);
        /// <summary>
        /// Deletes the asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> DeleteAsync(Guid id, string customerCode = null);

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        Task<int> BulkDeleteAsync(IEnumerable<int> ids, string customerCode = null);
        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        Task<int> BulkDeleteAsync(IEnumerable<long> ids, string customerCode = null);
        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        Task<int> BulkDeleteAsync(IEnumerable<Guid> ids, string customerCode = null);

        /// <summary>
        /// Softs the delete asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> SoftDeleteAsync(int id, string customerCode = null);
        /// <summary>
        /// Softs the delete asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        Task<int> SoftDeleteAsync(Guid id, string customerCode = null);
        /// <summary>
        /// Anies the asynchronous.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<bool> AnyAsync(Expression<Func<T, bool>> predicatExpression, string customerCode = null);
        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        Task<IEnumerable<T>> GetAllAsync(string customerCode = null);
        /// <summary>
        /// Gets the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        Task<T> GetByIdAsync(int id, string customerCode = null);
        /// <summary>
        /// Gets the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        Task<T> GetByIdAsync(long id, string customerCode = null);
        /// <summary>
        /// Firsts the or default asynchronous.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicatExpression, string customerCode = null);
        /// <summary>
        /// Finds the asynchronous.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicatExpression, string customerCode = null);
        /// <summary>
        /// Finds the top asynchronous.
        /// </summary>
        /// <param name="recordCount">The record count.</param>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDesc">if set to <c>true</c> [order by desc].</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        Task<IEnumerable<T>> FindTopAsync(int recordCount, Expression<Func<T, bool>> predicatExpression, string orderBy, bool orderByDesc = true, string customerCode = null);

        /// <summary>
        /// Finds the data by pagination.
        /// </summary>
        /// <param name="pageSize">The number of records per page.</param>
        /// <param name="pageNumber">The number of the page in the paginated collection.</param>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDesc">if set to <c>true</c> [order by desc].</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        Task<IEnumerable<T>> FindByPagination(int pageSize, int pageNumber, Expression<Func<T, bool>> predicatExpression, string orderBy, bool orderByDesc = true, string customerCode = null);

        /// <summary>
        /// Finds the data to queue asynchronous.
        /// </summary>
        /// <param name="recordCount">The record count.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        Task<IEnumerable<T>> FindDataToQueueAsync(int recordCount, string customerCode = null);

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.String.</returns>
        string GetConnectionString(string customerCode=null);
        #endregion

        #region "Single Connection/Request Implementation"

        void SetConnection(string customerCode);
        T CreateInTransaction(T entity);
        int UpdateInTransaction(T entity);
        IEnumerable<T> FindInTransation(Expression<Func<T, bool>> predicatExpression);
        T FirstOrDefaultInTransaction(Expression<Func<T, bool>> predicatExpression);

        #endregion
    }
}

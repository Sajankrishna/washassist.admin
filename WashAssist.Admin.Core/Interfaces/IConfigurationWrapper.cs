﻿using Microsoft.Extensions.Configuration;

namespace WashAssist.Admin.Core.Helper
{
    public interface IConfigurationWrapper
    {
        T GetValueOverride<T>(IConfiguration configuration, string section, string value);
    }
}
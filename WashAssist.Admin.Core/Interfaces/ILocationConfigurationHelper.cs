﻿using System;

namespace WashAssist.Admin.Core.Interfaces
{
    public interface ILocationConfigurationHelper
    {
        System.Threading.Tasks.Task<dynamic> GetLocationSpecificSettings(string siteCode, string tableName, Guid locationId, dynamic sites);
        System.Threading.Tasks.Task<dynamic> GetLocationSpecificSettingsSingleService(string siteCode, string tableName, Guid locationId, Guid tableId, object dynamicData);
    }
}
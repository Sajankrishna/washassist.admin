﻿using System;

namespace WashAssist.Admin.Core.Interfaces
{
    public interface IDapperUnitOfWork<T> : IDisposable where T : class
    {
        IRepository<T> GetRepository(string customerCode = null);
        IComplexTypeRepository<T> GetSpRepository();
    }
}

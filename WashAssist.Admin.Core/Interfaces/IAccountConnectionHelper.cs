﻿using WashAssist.Admin.Models;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace WashAssist.Admin.Core.Interfaces
{
    public interface IAccountConnectionHelper
    {
        /// <summary>
        /// Gets Insight Account Data from Customer Code
        /// Gets: EmailAddress,SMSProviderKey, SMSProviderNumber,FromEmail,EmailDomainName, SMSProviderTocken
        /// Returns: InsightAccount Object with above fields
        /// </summary>
        Task<InsightAccounts> GetAccountData(string customerCode);
        Task<List<InsightAccounts>> GetAllAccountData();
    }
}

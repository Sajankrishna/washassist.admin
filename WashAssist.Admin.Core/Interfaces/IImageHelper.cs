﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WashAssist.Admin.Core.Interfaces
{
    public interface IImageHelper
    {
        Task UploadVehicleImage(string imageName, string customerCode, Stream imagefile, string floder);

         Task<Stream> GetVehicleImage(string file, string customerCode, string imageDirectory);
    }
}

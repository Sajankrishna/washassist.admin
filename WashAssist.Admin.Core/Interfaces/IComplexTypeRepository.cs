﻿// ***********************************************************************
// Assembly         : WashAssist.Admin.Core
// Author           : Sajeev SL
// Created          : 06-20-2019
//
// Last Modified By : Sajeev SL
// Last Modified On : 02-12-2020
// ***********************************************************************
// <copyright file="IComplexTypeRepository.cs" company="WashAssist.Admin.Core">
//     Copyright (c) Micrologic Associates Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WashAssist.Admin.Core.Interfaces
{
    /// <summary>
    /// Interface IComplexTypeRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IComplexTypeRepository<T> where T : class
    {
        /// <summary>
        /// Executes the procedure.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        IEnumerable<T> ExecuteProcedure(string procName, Dictionary<string, string> parameters, string customerCode);
        /// <summary>
        /// Runs the procedure.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        void RunProcedure(string procName, Dictionary<string, string> parameters, string customerCode);
        /// <summary>
        /// Gets the paginated data.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRows">The total rows.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        IEnumerable<T> GetPaginatedData(string procName, int pageNo, int pageSize, out long totalRows, string customerCode);
        /// <summary>
        /// Gets the multi result set data.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="action">The action.</param>
        /// <param name="customerCode">The customer code.</param>
        void GetMultiResultSetData(string procName, Dictionary<string, string> parameters, Action<object> action, string customerCode);
        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.String.</returns>
        string GetConnectionString(string customerCode);

        /// <summary>
        /// Executes the procedure asynchronous.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        Task<IEnumerable<T>> ExecuteProcedureAsync(string procName, Dictionary<string, string> parameters, string customerCode);
        /// <summary>
        /// Runs the procedure asynchronous.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task.</returns>
        Task RunProcedureAsync(string procName, Dictionary<string, string> parameters, string customerCode);
        /// <summary>
        /// Gets the paginated data asynchronous.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;Tuple&lt;System.Int64, IEnumerable&lt;T&gt;&gt;&gt;.</returns>
        Task<Tuple<long, IEnumerable<T>>> GetPaginatedDataAsync(string procName, int pageNo, int pageSize, string customerCode);
        /// <summary>
        /// Gets the multi result set data asynchronous.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="action">The action.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task.</returns>
        Task GetMultiResultSetDataAsync(string procName, Dictionary<string, string> parameters, Action<object> action, string customerCode);
    }
}

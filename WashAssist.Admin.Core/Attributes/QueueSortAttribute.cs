﻿using System;

namespace WashAssist.Admin.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class QueueSortAttribute : Attribute
    {
    }
}

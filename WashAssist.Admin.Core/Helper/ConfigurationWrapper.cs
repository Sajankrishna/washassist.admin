﻿using WashAssist.Admin.Core.Helper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace WashAssist.Admin.Core.Interfaces
{
    public class ConfigurationWrapper : IConfigurationWrapper
    {
        public T GetValueOverride<T>(IConfiguration configuration, string section, string value)
        {
            return configuration.GetSection(section).GetValue<T>(value);
        }
    }
}

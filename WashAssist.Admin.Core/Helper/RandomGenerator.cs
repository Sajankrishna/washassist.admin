﻿using System;
using System.Text;
using System.Threading;

namespace WashAssist.Admin.Core.Helper
{
    public static class RandomGenerator
    {
        // Generate a random number between two numbers    
        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        // Generate a random string with a given size    
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        // Generate a random password    
        public static string RandomBarcode()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(2, true));
            builder.Append(RandomNumber(1000, 2999));
            Thread.Sleep(25);
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(3000, 6999));
            Thread.Sleep(25);
            builder.Append(RandomString(2, true));
            builder.Append(RandomNumber(7000, 9999));
            return builder.ToString();
        }
    }
}

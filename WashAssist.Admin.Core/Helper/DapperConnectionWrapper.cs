﻿using Dapper;
using WashAssist.Admin.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace WashAssist.Admin.Core.Helper
{
    public class DapperConnectionWrapper : IDapperConnectionWrapper
    {
        public async Task<T> QueryFirstOrDefaultAsyncOverride<T>(IDbConnection connection, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            return await connection.QueryFirstOrDefaultAsync<T>(sql, param, commandType: CommandType.StoredProcedure);
        }
    }
}

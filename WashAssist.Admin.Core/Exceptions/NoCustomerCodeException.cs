﻿using System;

namespace WashAssist.Admin.Core.Exceptions
{
    public class NoCustomerCodeException : Exception
    {
        public NoCustomerCodeException(string message) : base(message)
        {
        }
    }
}

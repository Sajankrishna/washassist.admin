﻿using System;

namespace WashAssist.Admin.Core.Exceptions
{
    /// <summary>
    /// Bad Customer Code Exception Class
    /// </summary>
    public class BadCustomerCodeException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public string BadCustomerCode { get; set; }

        /// <summary>
        /// Bad Customer Code Exception
        /// </summary>
        /// <param name="message"></param>
        public BadCustomerCodeException(string message) : base(message)
        {
        }

        /// <summary>
        /// Bad Customer Code Exception
        /// </summary>
        /// <param name="message">Message Description</param>
        /// <param name="badCustomerCode">Customer Code</param>
        public BadCustomerCodeException(string message, string badCustomerCode) : base(message)
        {
            BadCustomerCode = badCustomerCode;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WashAssist.Admin.Core.Exceptions
{
    public class BadLocationIdException : Exception
    {
        public string BadLocationId { get; set; }

        public BadLocationIdException() : base("Location Not Found")
        {
        }

    }
}

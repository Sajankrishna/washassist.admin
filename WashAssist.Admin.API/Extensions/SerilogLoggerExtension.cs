﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Exceptions;
using System;

namespace WashAssist.Admin.Extensions
{
    public static class SerilogLoggerExtension
    {
        public static void Initialize(ILoggerFactory loggerFactory, IConfiguration configuration)
        {
            var mlEnvironment = configuration.GetValue<string>("MLEnvironment");
            var productName = "MicrologicAPI";

            string logFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}\\logs";
            string logFile = $"{logFilePath}\\{productName}-{mlEnvironment}-.txt";

            Log.Logger = new LoggerConfiguration()
                            .ReadFrom.Configuration(configuration)
                            .Enrich.FromLogContext()
                            .Enrich.WithEnvironmentUserName()
                            .Enrich.WithExceptionDetails()
                            //.WriteTo.RollingFile(pathFormat: "logs\\log-{Date}.log")
                            .CreateLogger();

            loggerFactory.WithFilter(new FilterLoggerSettings
            {
                {"Trace",LogLevel.Trace },
                {"Default", LogLevel.Trace},
                {"Microsoft", LogLevel.Warning},
                {"System", LogLevel.Warning}
            })
            .AddSerilog();
        }
    }
}

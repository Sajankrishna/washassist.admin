﻿using WashAssist.Admin.Core.Interfaces;
using WashAssist.Admin.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class WebApiServiceCollectionExtensions
    {
        //public static void UseSqlTableDependency<T>(this IApplicationBuilder services, IConfiguration configuration)
        //   where T : IDatabaseSubscription
        //{
        //    var serviceProvider = services.ApplicationServices;
        //}

        public static IMvcCoreBuilder AddWebApi(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            // Add SignalR
            services.AddSignalR();

            var builder = services.AddMvcCore();

            builder.AddAuthorization();

            builder.AddFormatterMappings();

            builder.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            return builder;
        }

        public static IMvcCoreBuilder AddWebApi(this IServiceCollection services, Action<MvcOptions> setupAction, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            if (setupAction == null) throw new ArgumentNullException(nameof(setupAction));

            var builder = services.AddWebApi(configuration);
            builder.Services.Configure(setupAction);

            return builder;
        }
    }
}

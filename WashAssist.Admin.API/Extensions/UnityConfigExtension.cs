﻿using WashAssist.Admin.Core.Helper;
using WashAssist.Admin.Core.Interfaces;
using WashAssist.Admin.DapperDataAccess;
using Microsoft.Practices.Unity;
using WashAssist.Admin.Helpers;

namespace WashAssist.Admin.Extensions
{
    public static class UnityConfig
    {
        public static void Register(IUnityContainer container)
        {
            //container.RegisterType<IConnectionFactory>(s => new ConnectionFactory(GetConnectionStrings(configuration)));

            container.RegisterType(typeof(IConnectionFactory), typeof(ConnectionFactory), new ContainerControlledLifetimeManager());
            container.RegisterType(typeof(IConnectionHelper), typeof(ConnectionHelper), new ContainerControlledLifetimeManager());
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            container.RegisterType(typeof(IComplexTypeRepository<>), typeof(ComplexTypeRepository<>));
            container.RegisterType(typeof(IDapperUnitOfWork<>), typeof(UnitOfWork<>));

            container.RegisterType<IAccountConnectionHelper, AccountConnectionHelper>();
            container.RegisterType<IDapperConnectionWrapper, DapperConnectionWrapper>();
            container.RegisterType<IConfigurationWrapper, ConfigurationWrapper>();

        }
    }
}

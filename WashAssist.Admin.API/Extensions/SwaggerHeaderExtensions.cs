﻿using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;

namespace WashAssist.Admin.Extensions
{
    public class SwaggerHeaderExtensions : IOperationFilter
    {
        private readonly IConfiguration _configuration;

        public SwaggerHeaderExtensions(IConfiguration configuration) => _configuration = configuration;

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "X-Authentication-Token",
                In = ParameterLocation.Header,
                Description = "Access Token",
                Required = true
            });

        }
    }
}

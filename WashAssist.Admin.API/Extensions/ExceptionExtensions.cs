﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace WashAssist.Admin.Extensions
{
    public static class ExceptionExtensions 
    {
        public static string GetFullMessage(this Exception ex)
        {
            //if (Logging.LoggerFactory.LoggingDisabled==false)
            //    Serilog.Log.Error("An Error Occured", ex);
            
            return ex.InnerException == null
                 ? ex.Message
                 : ex.Message + " --> " + ex.InnerException.GetFullMessage();
        }

        public static string getMethodUsed([CallerMemberName] string errMethod = "")
        {
            return errMethod;
        }
    }
}

﻿namespace WashAssist.Admin.Logging
{
    using Serilog.Core;
    using Serilog.Events;
    using System.Runtime.CompilerServices;

    public class LogEnricherAccessTokenInfo : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("AccessToken", LoggerFactory.AccessToken));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("DbServer", LoggerFactory.DbServer));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("DbCatalog", LoggerFactory.DbCatalog));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("LocationId", LoggerFactory.LocationId));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("CustomerCode", LoggerFactory.CustomerCode));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("UpdateBy", LoggerFactory.UpdateBy));
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("VersionNumber", LoggerFactory.VersionNumber));
        }

    }
}

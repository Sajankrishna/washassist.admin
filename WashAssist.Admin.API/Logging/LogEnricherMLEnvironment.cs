﻿namespace WashAssist.Admin.Logging
{
    using Serilog.Core;
    using Serilog.Events;
    using System.Runtime.CompilerServices;

    public class LogEnricherMLEnvironment : ILogEventEnricher
    {
        LogEventProperty _cachedProperty;
        public const string PropertyName = "MLEnvironment";

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddPropertyIfAbsent(GetLogEventProperty(propertyFactory));
        }

        private LogEventProperty GetLogEventProperty(ILogEventPropertyFactory propertyFactory)
        {
            if (_cachedProperty == null)
            {
                _cachedProperty = CreateProperty(propertyFactory);
            }
            return _cachedProperty;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static LogEventProperty CreateProperty(ILogEventPropertyFactory propertyFactory)
        {
            var value = LoggerFactory.MLEnvironment ?? "Local";
            return propertyFactory.CreateProperty(PropertyName, value);
        }
    }
}

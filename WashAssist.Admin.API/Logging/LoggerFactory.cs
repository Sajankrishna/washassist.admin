﻿namespace WashAssist.Admin.Logging
{
    using Microsoft.Extensions.Configuration;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;
    using Serilog.Formatting.Compact;
    using System;

    public static class LoggerFactory
    {
        public static string MLEnvironment { get; set; }

        public static string SeqServer { get; set; }

        public static string UserId { get; set; }

        public static string ProductName { get; set; }

        public static string DbServer { get; set; }
        public static string DbCatalog { get; set; }

        public static Guid LocationId { get; set; }
        public static string CustomerCode { get; set; }
        public static string UpdateBy { get; set; }             
        public static string VersionNumber { get; set; }
        public static bool LoggingDisabled { get; set; }
        public static LogEventLevel SeqMinimumLevel { get; set; }
        public static string AccessToken { get; internal set; }

        public static bool InstantiateLogger(IConfiguration Configuration)
        {
            MLEnvironment = Configuration.GetValue<string>("MLEnvironment");
            SeqServer = Configuration.GetValue<string>("SeqServer");
            string seqMinimumLevel = Configuration.GetValue<string>("SerilogMinimumLevel") ?? "";
            SetMinimumLevelFromConfig(seqMinimumLevel);
            ProductName = "MicrologicAPI";
            var seqApiKey = Configuration.GetValue<string>("SeqApiKey");

            string logFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}\\logs";
            string logFile = $"{logFilePath}\\{ProductName}-{MLEnvironment}-.txt";
           
            if (String.IsNullOrWhiteSpace(SeqServer))
            {
                LoggingDisabled = true;
                var log = new LoggerConfiguration()
                    .MinimumLevel.Error()
                    //.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                    //.MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                    //.MinimumLevel.Override("Serilog.AspNetCore", LogEventLevel.Warning)
                    //.Enrich.WithMLEnvironment()
                    //.Enrich.WithProductName()
                    //.Enrich.FromLogContext()
                    //.Enrich.WithAccessTokenInfo()
                    //.WriteTo.File(new CompactJsonFormatter(), logFile, rollingInterval: RollingInterval.Day, retainedFileCountLimit: 7)
                    .CreateLogger();
                Log.Logger = log;
            }
            else
            {
                if (SeqServer=="LOCALFILEONLY")
                {
                    LoggingDisabled = false;
                    var log = new LoggerConfiguration()
                        .MinimumLevel.Is(SeqMinimumLevel)
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                        .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                        .MinimumLevel.Override("Serilog.AspNetCore", LogEventLevel.Warning)
                        .Enrich.WithMLEnvironment()
                        .Enrich.WithProductName()
                        .Enrich.FromLogContext()
                        .Enrich.WithAccessTokenInfo()
                        //.WriteTo.Seq(SeqServer)
                        .WriteTo.File(new CompactJsonFormatter(), logFile, rollingInterval: RollingInterval.Day, retainedFileCountLimit: 7)
                        .CreateLogger();
                    Log.Logger = log;

                    Log.Information($"Logging Instantiated for {ProductName}");
                } else
                {
                    LoggingDisabled = false;
                    var levelSwitch = new LoggingLevelSwitch();
                    levelSwitch.MinimumLevel = SetMinimumLevelFromConfig(seqMinimumLevel);
                    var log = new LoggerConfiguration()
                        .Enrich.WithMLEnvironment()
                        .Enrich.WithProductName()
                        .Enrich.FromLogContext()
                        .Enrich.WithAccessTokenInfo()
                        .WriteTo.Seq(SeqServer,
                                    apiKey: seqApiKey,
                                    controlLevelSwitch: levelSwitch)
                        .WriteTo.File(new CompactJsonFormatter(), logFile, rollingInterval: RollingInterval.Day, retainedFileCountLimit: 7)
                        .CreateLogger();
                    Log.Logger = log;

                    Log.Information($"Logging Instantiated for {ProductName}");
                }

            }
            
            return true;
        }

        private static LogEventLevel SetMinimumLevelFromConfig(string minimumLevel)
        {
            switch (minimumLevel.ToLower())
            {
                case "debug": return LogEventLevel.Debug;
                case "verbose": return LogEventLevel.Verbose; 
                case "information": return LogEventLevel.Information;                 
                case "warning": return LogEventLevel.Warning; 
                case "error": return LogEventLevel.Error; 
                case "fatal": return LogEventLevel.Fatal;                 
                default: return LogEventLevel.Verbose; 
            }
        }
    }
}


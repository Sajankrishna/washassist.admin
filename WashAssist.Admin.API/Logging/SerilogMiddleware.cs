﻿
using Microsoft.AspNetCore.Http;
using Serilog;
using Serilog.Context;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WashAssist.Admin.Logging
{
    public class SerilogMiddleware
    {
        static readonly ILogger Log = Serilog.Log.ForContext<SerilogMiddleware>();

        readonly RequestDelegate _next;

        public SerilogMiddleware(RequestDelegate next)
        {
            if (next == null) throw new ArgumentNullException(nameof(next));
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {

            //var headers = httpContext.Request.Headers;
            //if (headers.ContainsKey(AppConstants.AUTHENTICATION_TOKEN))
            //{
            //    try
            //    {
            //        var vHeaderValue = httpContext.Request.Headers[AppConstants.AUTHENTICATION_TOKEN];
            //        var encryptetransactioncode = vHeaderValue.ToString();

            //        var decryptetransactioncode = CryptoHelper.Decrypt(encryptetransactioncode);

            //        var words = decryptetransactioncode.Split(',');
            //        var datestring = words[1];
            //        var username = words[0];
            //        var siteCode = words[2];
            //        var updateBy = words[3];
            //        var LocationId = words[4];
            //        if (words.Length > 5)
            //        {
            //            var userClaim = new UserClaim
            //            {
            //                ClaimType = words[5]
            //            };
            //        }

            //        LoggerFactory.LocationId = Guid.Parse(LocationId);
            //        LoggerFactory.CustomerCode = siteCode;
            //        LoggerFactory.UpdateBy = updateBy;

            //    }
            //    catch (Exception ex)
            //    {
                    
            //    }

            //}

            await _next(httpContext);

            //using (LogContext.PushProperty("NewProp", "NewValue"))
            //{
            //    Log.Information("From the Middleware");
            //    await _next(httpContext);
            //}


        }
    }
}

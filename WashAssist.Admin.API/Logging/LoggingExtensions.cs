﻿namespace WashAssist.Admin.Logging
{
    using Serilog;
    using Serilog.Configuration;
    using System;

   public static class LoggingExtensions
    {
        public static LoggerConfiguration WithMLEnvironment(
            this LoggerEnrichmentConfiguration enrich)
        {
            if (enrich == null)
            {
                throw new ArgumentNullException(nameof(enrich));
            }
            return enrich.With<LogEnricherMLEnvironment>();
        }

        public static LoggerConfiguration WithProductName(
            this LoggerEnrichmentConfiguration enrich)
        {
            if (enrich == null)
            {
                throw new ArgumentNullException(nameof(enrich));
            }
            return enrich.With<LogEnricherProductName>();
        }

        public static LoggerConfiguration WithAccessTokenInfo(
            this LoggerEnrichmentConfiguration enrich)
        {
            if (enrich == null)
            {
                throw new ArgumentNullException(nameof(enrich));
            }
            return enrich.With<LogEnricherAccessTokenInfo>();
        }
    }
}

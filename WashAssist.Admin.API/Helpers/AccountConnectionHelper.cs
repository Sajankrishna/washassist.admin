﻿using LazyCache;
using WashAssist.Admin.Core.Interfaces;
using WashAssist.Admin.DapperDataAccess;
using WashAssist.Admin.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WashAssist.Admin.Helpers
{
    public class AccountConnectionHelper : IAccountConnectionHelper
    {
        private readonly IAppCache _cache;
        private readonly IConfiguration _configuration;

        public AccountConnectionHelper(IAppCache cache, IConfiguration configuration)
        {
            _cache = cache;
            _configuration = configuration;
        }

        public async Task<InsightAccounts> GetAccountData(string customerCode)
        {
            var connectionString = _configuration.GetConnectionString("InsightEnterprise");

            return await _cache.GetOrAddAsync(customerCode, async () =>
            {
                using (var connection = new SqlConnection(connectionString))
                {
#pragma warning disable CA2100
                    using (var command = new SqlCommand($"select EmailAddress, EmailProviderKey, SMSProviderKey, SMSProviderNumber, FromEmail, EmailDomainName, SMSProviderTocken, isPaymentLive, paymentMode, SMSOTPMessage, bPortalRefundEnabled, bSurchargeFirstMonthEnabled  FROM Accounts WHERE InsightCustomerCode = '{customerCode}'", connection))
#pragma warning restore CA2100
                    {
                        connection.Open();
                        var sqlReader = await command.ExecuteReaderAsync();
                        if (await sqlReader.ReadAsync())
                        {

                            var insightAccount = new InsightAccounts
                            {
                                EmailAddress = sqlReader.IsDBNull(0) == true ? "" : await sqlReader.GetFieldValueAsync<string>(0),
                                EmailProviderKey = sqlReader.IsDBNull(1) == true ? "" : await sqlReader.GetFieldValueAsync<string>(1),
                                SMSProviderKey = sqlReader.IsDBNull(2) == true ? "" : await sqlReader.GetFieldValueAsync<string>(2),
                                SMSProviderNumber = sqlReader.IsDBNull(3) == true ? "" : await sqlReader.GetFieldValueAsync<string>(3),
                                FromEmail = sqlReader.IsDBNull(4) == true ? "" : await sqlReader.GetFieldValueAsync<string>(4),
                                EmailDomainName = sqlReader.IsDBNull(5) == true ? "" : await sqlReader.GetFieldValueAsync<string>(5),
                                SMSProviderTocken = sqlReader.IsDBNull(6) == true ? "" : await sqlReader.GetFieldValueAsync<string>(6),
                                isPaymentLive = sqlReader.IsDBNull(7) == true ? false : await sqlReader.GetFieldValueAsync<bool>(7),
                                paymentMode = sqlReader.IsDBNull(8) == true ? "" : await sqlReader.GetFieldValueAsync<string>(8),
                                SMSOTPMessage = sqlReader.IsDBNull(9) == true ? "" : await sqlReader.GetFieldValueAsync<string>(9),
                                bPortalRefundEnabled = sqlReader.IsDBNull(10) == true ? true : await sqlReader.GetFieldValueAsync<bool>(10),
                                bSurchargeFirstMonthEnabled = sqlReader.IsDBNull(11) == true ? false : await sqlReader.GetFieldValueAsync<bool>(11)
                            };
                            return insightAccount;
                        }
                        return null;
                    }
                }
            }, TimeSpan.FromMinutes(5));
        }


        public async Task<List<InsightAccounts>> GetAllAccountData()
        {
            var connectionString = _configuration.GetConnectionString("InsightEnterprise");


            using (var connection = new SqlConnection(connectionString))
            {
#pragma warning disable CA2100
                using (var command = new SqlCommand($"select InsightCustomerCode, isPaymentLive, DatabaseName  FROM Accounts", connection))
#pragma warning restore CA2100
                {
                    connection.Open();
                    var sqlReader = await command.ExecuteReaderAsync();
                    List<InsightAccounts> InsightAccountsList = new List<InsightAccounts>();

                    int count = 0;
                    foreach (System.Data.Common.DbDataRecord dr in sqlReader)
                    {
                        var insightAccount = new InsightAccounts();
                        insightAccount.InsightCustomerCode = sqlReader.IsDBNull(0) == true ? "" : await sqlReader.GetFieldValueAsync<string>(0);
                        insightAccount.isPaymentLive = sqlReader.IsDBNull(1) == true ? false : await sqlReader.GetFieldValueAsync<bool>(1);
                        insightAccount.DatabaseName = sqlReader.IsDBNull(2) == true ? "" : await sqlReader.GetFieldValueAsync<string>(2);

                        InsightAccountsList.Add(insightAccount);
                    }
                    return InsightAccountsList;
                }
            }
        }

    }
}

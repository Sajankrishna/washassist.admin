﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WashAssist.Admin.Helpers
{
    public static class CustomerCodeToNameHelper
    {
        public static string CustomerFulName(string customerCode)
        {
            customerCode = customerCode != null && customerCode != "" ? customerCode.ToLower() : "";
            string customer = string.Empty;
            switch (customerCode)
            {
                case "hoffmancwstg":
                    customer = "Hoffman Car Wash";
                    break;

                case "hoffmancw":
                    customer = "Hoffman Car Wash";
                    break;

                case "micrologiccw":
                    customer = "Micrologic Car Wash";
                    break;

                case "expresswc":
                    customer = "Express Wash Concepts";
                    break;

                case "flcw":
                    customer = "Finishline Car Wash";
                    break;

                case "foamwash":
                    customer = "Foam and Wash Car Wash";
                    break;

                case "russellspeeders":
                    customer = "Russell Speeders Car Wash";
                    break;

                case "demo":
                    customer = "Micrologic Car Wash";
                    break;

                case "kevincw":
                    customer = "Kevin Car Wash";
                    break;

                case "waterdrops":
                    customer = "Waterdrops Car Wash";
                    break;

                case "bubblebarnexpresscw":
                    customer = "Bubble Barn Express Car Wash";
                    break;

                case "sudzeecw":
                    customer = "Waterworks Car Wash";
                    break;

                case "greencleancw":
                    customer = "Green Clean Car Wash";
                    break;

                case "blueskycw":
                    customer = "Bluesky Car Wash";
                    break;

                default:
                    customer = "";
                    break;
            }

            return   customer ;
        }
    }
}

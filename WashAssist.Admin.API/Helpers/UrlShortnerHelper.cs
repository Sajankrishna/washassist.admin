﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WashAssist.Admin.Helpers
{
    public class UrlShortnerHelper
    {
        public static string ShortenUrl(string url)
        {
            string tinyUrl = string.Format("http://tinyurl.com/api-create.php?url={0}", url);
            using (WebClient webclient = new WebClient())
            {
                return webclient.DownloadString(tinyUrl);
            }
        }
    }
}

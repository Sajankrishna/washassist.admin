﻿using LazyCache;
using WashAssist.Admin.DapperDataAccess;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WashAssist.Admin.Logging;

namespace WashAssist.Admin.Helpers
{
    public class ConnectionHelper : IConnectionHelper
    {
        private readonly IAppCache _cache;
        private readonly IConfiguration _configuration;

        private static string _CUSTOMER_CONNECTION_STRING = "Server={0};Database={1};user id={2};password={3};MultipleActiveResultSets=False;Application Name=WA-{4};MultiSubnetFailover=True;";
        private static string _CUSTOMER_CONNECTION_STRING_NONMIRRORED = "Server={0};Database={1};user id={2};password={3};MultipleActiveResultSets=False;Application Name=WA-{4};";

        public ConnectionHelper(IAppCache cache, IConfiguration configuration)
        {
            _cache = cache;
            _configuration = configuration;
        }

        public async Task<string> GetConnectingString(string customerCode)
        {
            var connectionString = _configuration.GetConnectionString("InsightEnterprise");

            //LogContext.PushProperty("DBConnectionStringInfo", "DBConnectionStringInfoVALUE");

            //return await _cache.GetOrAddAsync(customerCode, async () =>
            //{
                using (var connection = new SqlConnection(connectionString))
                {
#pragma warning disable CA2100
                    using (var command = new SqlCommand($"select ServerIp, DatabaseName, UserId, Password FROM Accounts WHERE InsightCustomerCode = '{customerCode}'", connection))
#pragma warning restore CA2100
                    {
                    string formattedConnectionString;
                        connection.Open();
                        var sqlReader = await command.ExecuteReaderAsync();
                        if (await sqlReader.ReadAsync())
                        {
                            var serverip = await sqlReader.GetFieldValueAsync<string>(0);

                        ////DEBUGCODE - IF FOUND LIVE PLEASE REMOVE AAN
                        //if (serverip == "StagListener1.mlogic.local")
                        //{
                        //    serverip = "34.200.238.92";
                        //}
                        ////DEBUGCODE - IF FOUND LIVE PLEASE REMOVE AAN

                        if (IsDigitsOnly(serverip))
                            {

//                                //if (serverip == ".")
//                                //    serverip = "mlastgapi.washassist.com";
//#if DEBUG
//                                if (serverip == ".") // TODO Get rid of this
//                                    serverip = "34.195.151.87"; // TODO Get rid of this
//#endif
                                formattedConnectionString = string.Format(_CUSTOMER_CONNECTION_STRING_NONMIRRORED, serverip, await sqlReader.GetFieldValueAsync<string>(1), await sqlReader.GetFieldValueAsync<string>(2), await sqlReader.GetFieldValueAsync<string>(3), customerCode);
                                LoggerFactory.DbServer = serverip;
                                LoggerFactory.DbCatalog = await sqlReader.GetFieldValueAsync<string>(1);

                                //Log.ForContext("ConnectionString", formattedConnectionString).Information($"DB Connection Information after ConnectionHelper");
                                return formattedConnectionString;
                            }
                                formattedConnectionString = string.Format(_CUSTOMER_CONNECTION_STRING, serverip, await sqlReader.GetFieldValueAsync<string>(1), await sqlReader.GetFieldValueAsync<string>(2), await sqlReader.GetFieldValueAsync<string>(3), customerCode);
                                LoggerFactory.DbServer = serverip;
                                LoggerFactory.DbCatalog = await sqlReader.GetFieldValueAsync<string>(1);

                                //Log.ForContext("ConnectionString", formattedConnectionString).Information($"DB Connection Information after ConnectionHelper");
                                return formattedConnectionString;
                        }
                        return null;
                    }
                }
            //}, TimeSpan.FromMinutes(5));
        }

        private bool IsDigitsOnly(string str)
        {
            str = str.Replace(".", string.Empty).Trim();
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}

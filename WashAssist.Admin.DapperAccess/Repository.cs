﻿// ***********************************************************************
// Assembly         : WashAssist.Admin.DapperDataAccess
// Author           : Sajeev SL
// Created          : 02-06-2020
//
// Last Modified By : Sajeev SL
// Last Modified On : 02-24-2021
// ***********************************************************************
// <copyright file="Repository.cs" company="WashAssist.Admin.DapperDataAccess">
//     Copyright (c) Micrologic Associates Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using Dapper;
using DapperParameters;
using WashAssist.Admin.Core.Attributes;
using WashAssist.Admin.Core.Interfaces;
using Microsoft.Data.SqlClient;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WashAssist.Admin.DapperDataAccess
{
    /// <summary>
    /// Class Repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="WashAssist.Admin.Core.Interfaces.IRepository{T}" />
    public class Repository<T> : IRepository<T> where T : class
    {
        private bool isDisposed;
        /// <summary>
        /// The table
        /// </summary>
        private string _table;
        /// <summary>
        /// The table identifier column
        /// </summary>
        private List<string> _tableIdentifierColumn;
        /// <summary>
        /// The table identity columns
        /// </summary>
        private List<string> _tableIdentityColumns;
        /// <summary>
        /// The table columns
        /// </summary>
        private List<string> _tableColumns;
        /// <summary>
        /// The table ignore on update columns
        /// </summary>
        private List<string> _tableIgnoreOnUpdateColumns;
        /// <summary>
        /// The connection factory
        /// </summary>
        protected IConnectionFactory _connectionFactory;
        protected IDbConnection _connection;
        protected string _customerCode;
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}" /> class.
        /// </summary>
        /// <param name="connectionFactory">The connection factory.</param>
        public Repository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
            Setup();
        }

        /// <summary>
        /// Setups this instance.
        /// </summary>
        private void Setup()
        {
            _table = GetTableName();
            _tableIdentifierColumn = GetTableIdentifierColumn();
            _tableIdentityColumns = GetTableIdentityColumns();
            _tableColumns = GetTableColumns();
            _tableIgnoreOnUpdateColumns = GetTableIgnoreOnUpdateColumns();
            if (_tableIgnoreOnUpdateColumns == null)
                _tableIgnoreOnUpdateColumns = new List<string>();
            HasTableIdentity = true;

        }

        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <returns>System.String.</returns>
        private string GetTableName()
        {
            var type = typeof(T);
            var attribute = (TableAttribute)Attribute.GetCustomAttribute(type, typeof(TableAttribute));
            return attribute?.Name;
        }

        /// <summary>
        /// Gets the table identifier column.
        /// </summary>
        /// <returns>System.String.</returns>
        private List<string> GetTableIdentifierColumn()
        {
            return (typeof(T)
                    .GetProperties()
                    .Where(prop => (IdentifierAttribute)Attribute.GetCustomAttribute(prop, typeof(IdentifierAttribute)) != null)
                    .Select(prop => prop.Name)).ToList();
        }
        /// <summary>
        /// Gets the table identity columns.
        /// </summary>
        /// <returns>List&lt;System.String&gt;.</returns>
        private List<string> GetTableIdentityColumns()
        {
            return (typeof(T)
         .GetProperties()
         .Where(prop => (IdentityColumnAttribute)Attribute.GetCustomAttribute(prop, typeof(IdentityColumnAttribute)) != null)
         .Select(prop => prop.Name)).ToList();
        }
        /// <summary>
        /// Gets the table ignore on update columns.
        /// </summary>
        /// <returns>List&lt;System.String&gt;.</returns>
        private List<string> GetTableIgnoreOnUpdateColumns()
        {
            return (typeof(T)
                    .GetProperties()
                    .Where(prop => (IgnoreOnUpdateAttribute)Attribute.GetCustomAttribute(prop, typeof(IgnoreOnUpdateAttribute)) != null)
                    .Select(prop => prop.Name)).ToList();
        }

        /// <summary>
        /// Gets the table columns.
        /// </summary>
        /// <returns>List&lt;System.String&gt;.</returns>
        private List<string> GetTableColumns()
        {
            return typeof(T)
                    .GetProperties()
                    .Where(prop => !_tableIdentityColumns.Contains(prop.Name))
                    .Where(prop => !_tableIdentifierColumn.Contains(prop.Name))
                    .Where(prop => (ExcludeAttribute)Attribute.GetCustomAttribute(prop, typeof(ExcludeAttribute)) == null)
                    .Select(prop => prop.Name)
                    .ToList();
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has identity insert.
        /// </summary>
        /// <value><c>true</c> if this instance has identity insert; otherwise, <c>false</c>.</value>
        public bool HasIdentityInsert { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has table identity.
        /// </summary>
        /// <value><c>true</c> if this instance has table identity; otherwise, <c>false</c>.</value>
        public bool HasTableIdentity { get; set; }

        /// <summary>
        /// Creates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        public T Create(T entity, string customerCode = null)
        {
            string query = GenerateCreateQuery();

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var result = conn.Query<T>(query, entity, commandType: CommandType.Text);
                conn.Close();
                return result.First();
            }
        }

        /// <summary>
        /// create as an asynchronous operation.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        public async Task<T> CreateAsync(T entity, string customerCode = null)
        {
            string query = GenerateCreateQuery();
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var result = await conn.QueryAsync<T>(query, entity, commandType: CommandType.Text);
                conn.Close();
                return result.First();
            }
        }

        /// <summary>
        /// Bulks the import.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool BulkImport(List<T> entities, string customerCode = null)
        {
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var parameters = new DynamicParameters();
                parameters.AddTable("@queueData", "udtReplicationQueueForAdd", entities);
                conn.Query("api_InsertReplicationQueueData", parameters, commandType: CommandType.StoredProcedure);
                conn.Close();
            }

            return true;
        }

        /// <summary>
        /// bulk import as an asynchronous operation.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task.</returns>
        public async Task BulkImportAsync(List<T> entities, string customerCode = null)
        {
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                using (var dataTable = new DataTable())
                {
                    var columns = typeof(T).GetProperties().Select(p => new DataColumn(p.Name, Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType)).ToArray();
                    dataTable.Columns.AddRange(columns);
                    dataTable.AcceptChanges();
                    Type localType = typeof(T);
                    var tableName = ((TableAttribute)Attribute.GetCustomAttribute(localType, typeof(TableAttribute))).Name;
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = tableName
                    };
                    var rows = entities.Select(r =>
                    {
                        var row = dataTable.NewRow();
                        Array.ForEach(r.GetType().GetProperties().ToArray(), (p) =>
                        {
                            row[p.Name] = p.GetValue(r) ?? DBNull.Value;
                        });
                        return row;
                    });
                    foreach (var row in rows)
                    {
                        dataTable.Rows.Add(row);
                    }
                    dataTable.AcceptChanges();
                    if (dataTable.Rows.Count > 0)
                    {
                        await bulkCopy.WriteToServerAsync(dataTable);
                    }
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        public int Update(T entity, string customerCode = null)
        {
            var result = 0;

            var query = GenerateUpdateQuery();

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, entity, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        public async Task<int> UpdateAsync(T entity, string customerCode = null)
        {
            var result = 0;

            var query = GenerateUpdateQuery();

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, entity, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        public int Delete(int id, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        public int Delete(long id, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Softs the delete.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        public int SoftDelete(int id, string customerCode = null)
        {
            var result = 0;
            var query = $"UPDATE {_table} SET bDeleted = 1 WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// delete as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        public async Task<int> DeleteAsync(int id, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// delete as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        public async Task<int> DeleteAsync(long id, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// soft delete as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        public async Task<int> SoftDeleteAsync(int id, string customerCode = null)
        {
            var result = 0;
            var query = $"UPDATE {_table} SET bDeleted = 1 WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        public int Delete(Guid id, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// delete as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        public async Task<int> DeleteAsync(Guid id, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} = '@id'";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public int BulkDelete(IEnumerable<int> ids, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { ids }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public int BulkDeleteWithSp(IEnumerable<long> ids, string customerCode)
        {
            var result = 0;
            //var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            var completedData = new List<udtReplicationQueueIds>();
            ids.ToList().ForEach((item) =>
            {
                completedData.Add(new udtReplicationQueueIds
                {
                    lQueueId = item
                });
            });

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                //result = conn.Execute(query, new { ids }, commandType: CommandType.Text);
                var parameters = new DynamicParameters();
                parameters.AddTable("@queueData", "udtReplicationQueueIds", completedData);
                conn.Query("api_SetReplicationCompleted", parameters, commandType: CommandType.StoredProcedure);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public int BulkDelete(IEnumerable<long> ids, string customerCode)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { ids }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Bulks the delete.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public int BulkDelete(IEnumerable<Guid> ids, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { ids }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// bulk delete as an asynchronous operation.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public async Task<int> BulkDeleteAsync(IEnumerable<int> ids, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { ids }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// bulk delete as an asynchronous operation.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public async Task<int> BulkDeleteAsync(IEnumerable<long> ids, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { ids }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// bulk delete as an asynchronous operation.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public async Task<int> BulkDeleteAsync(IEnumerable<Guid> ids, string customerCode = null)
        {
            var result = 0;
            var query = $"DELETE FROM {_table} WHERE {_tableIdentifierColumn.First()} IN @ids";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { ids }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Softs the delete.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.Int32.</returns>
        public int SoftDelete(Guid id, string customerCode = null)
        {
            var result = 0;
            var query = $"UPDATE {_table} SET bDeleted = 1 WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Execute(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// soft delete as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Int32&gt;.</returns>
        public async Task<int> SoftDeleteAsync(Guid id, string customerCode = null)
        {
            var result = 0;
            var query = $"UPDATE {_table} SET bDeleted = 1 WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.ExecuteAsync(query, new { id }, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Anies the specified predicat expression.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool Any(Expression<Func<T, bool>> predicatExpression, string customerCode = null)
        {
            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT 1 FROM {_table} WITH (NOLOCK) WHERE {whereClause}";


            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var exists = (conn.Query<int>(query, commandType: CommandType.Text)).Any();
                conn.Close();
                return exists;
            }
        }

        /// <summary>
        /// any as an asynchronous operation.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicatExpression, string customerCode = null)
        {
            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT 1 FROM {_table} WITH (NOLOCK) WHERE {whereClause}";


            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var exists = (await conn.QueryAsync<int>(query, commandType: CommandType.Text)).Any();
                conn.Close();
                return exists;
            }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        public IEnumerable<T> GetAll(string customerCode)
        {
            IEnumerable<T> result;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK)";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Query<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }
        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public async Task<IEnumerable<T>> GetAllAsync(string customerCode)
        {
            IEnumerable<T> result;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK)";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.QueryAsync<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Finds the specified predicat expression.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        public IEnumerable<T> Find(Expression<Func<T, bool>> predicatExpression, string customerCode = null)
        {
            IEnumerable<T> result;

            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause}";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Query<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// find top as an asynchronous operation.
        /// </summary>
        /// <param name="recordCount">The record count.</param>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDesc">if set to <c>true</c> [order by desc].</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public IEnumerable<T> FindTop(int recordCount, Expression<Func<T, bool>> predicatExpression, string orderBy, bool orderByDesc = false, string customerCode = null)
        {
            IEnumerable<T> result;

            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var orderByOrder = orderByDesc ? "DESC" : "ASC";
            var query = $"SELECT TOP {recordCount} {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause} ORDER BY {orderBy} {orderByOrder}";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = conn.Query<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// find as an asynchronous operation.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicatExpression, string customerCode = null)
        {
            IEnumerable<T> result;

            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause}";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.QueryAsync<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// find top as an asynchronous operation.
        /// </summary>
        /// <param name="recordCount">The record count.</param>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDesc">if set to <c>true</c> [order by desc].</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public async Task<IEnumerable<T>> FindTopAsync(int recordCount, Expression<Func<T, bool>> predicatExpression, string orderBy, bool orderByDesc = false, string customerCode = null)
        {
            IEnumerable<T> result;

            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var orderByOrder = orderByDesc ? "DESC" : "ASC";
            var query = $"SELECT TOP {recordCount} {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause} ORDER BY {orderBy} {orderByOrder}";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.QueryAsync<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Finds the data by pagination.
        /// </summary>
        /// <param name="pageSize">The number of records per page.</param>
        /// <param name="pageNumber">The number of the page in the paginated collection.</param>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDesc">if set to <c>true</c> [order by desc].</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public async Task<IEnumerable<T>> FindByPagination(int pageSize, int pageNumber, Expression<Func<T, bool>> predicatExpression, string orderBy, bool orderByDesc = true, string customerCode = null)
        {
            IEnumerable<T> result;
            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var orderByOrder = orderByDesc ? "DESC" : "ASC";
            pageNumber = (pageNumber - 1) * pageSize;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause} ORDER BY {orderBy} {orderByOrder} OFFSET {pageNumber} ROWS FETCH NEXT {pageSize} ROWS ONLY";
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.QueryAsync<T>(query, commandType: CommandType.Text);
                conn.Close();
            }
            return result;
        }

        /// <summary>
        /// find data to queue as an asynchronous operation.
        /// </summary>
        /// <param name="recordCount">The record count.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public async Task<IEnumerable<T>> FindDataToQueueAsync(int recordCount, string customerCode = null)
        {
            IEnumerable<T> result;

            var query = $"SELECT TOP {recordCount} {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE dtReplicated IS NULL OR dtReplicated < dtCreated ORDER BY dtCreated DESC";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = await conn.QueryAsync<T>(query, commandType: CommandType.Text);
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        public T GetById(int id, string customerCode = null)
        {
            T result;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = (conn.Query<T>(query, new { id }, commandType: CommandType.Text)).FirstOrDefault();
                conn.Close();
            }

            return result;
        }
        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        public async Task<T> GetByIdAsync(int id, string customerCode = null)
        {
            T result;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = (await conn.QueryAsync<T>(query, new { id }, commandType: CommandType.Text)).FirstOrDefault();
                conn.Close();
            }

            return result;
        }
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        public T GetById(long id, string customerCode = null)
        {
            T result;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = (conn.Query<T>(query, new { id }, commandType: CommandType.Text)).FirstOrDefault();
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        public async Task<T> GetByIdAsync(long id, string customerCode = null)
        {
            T result;
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {_tableIdentifierColumn.First()} = @id";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                result = (await conn.QueryAsync<T>(query, new { id }, commandType: CommandType.Text)).FirstOrDefault();
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Firsts the or default.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>T.</returns>
        public T FirstOrDefault(Expression<Func<T, bool>> predicatExpression, string customerCode = null)
        {
            T result = null;

            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT TOP 1 {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause}";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var results = conn.Query<T>(query, commandType: CommandType.Text);
                if (results != null && results.Any())
                {
                    result = results.First();
                }
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// first or default as an asynchronous operation.
        /// </summary>
        /// <param name="predicatExpression">The predicat expression.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicatExpression, string customerCode = null)
        {
            T result = null;
            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT TOP 1 {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause}";

            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open();
                SqlConnection.ClearPool(conn);
                var results = await conn.QueryAsync<T>(query, commandType: CommandType.Text);
                if (results != null && results.Any())
                {
                    result = results.First();
                }
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// Generates the create query.
        /// </summary>
        /// <returns>System.String.</returns>

        private string GenerateCreateQuery()
        {
            var query =
                "INSERT " + _table + " (" + string.Join(", ", _tableColumns) + ") " +
                "VALUES (" + string.Join(", ", _tableColumns.Select(c => "@" + c)) + "); ";

            query += $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {_tableIdentifierColumn.First()} = SCOPE_IDENTITY();";

            if (HasIdentityInsert)
            {
                query = string.Empty;
                if (HasTableIdentity)
                {
                    query = $"SET IDENTITY_INSERT [dbo].[{_table}] ON; ";
                }
                query += "INSERT " + _table + " (" + string.Join(",", _tableIdentifierColumn) + "," + string.Join(", ", _tableColumns) + ") " +
                "VALUES (@" + string.Join(",@", _tableIdentifierColumn) + "," + string.Join(", ", _tableColumns.Select(c => "@" + c)) + "); ";
                if (HasTableIdentity)
                {
                    query += $" SET IDENTITY_INSERT [dbo].[{_table}] OFF;";
                }
                var whereClause = string.Empty;
                query += $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {string.Join(" AND ", _tableIdentifierColumn.Select(c => $"{c} = @{c}"))}";
            }

            return query;
        }

        /// <summary>
        /// Generates the update query.
        /// </summary>
        /// <returns>System.String.</returns>
        private string GenerateUpdateQuery()
        {
            var query =
                "UPDATE " + _table +
                " SET " + string.Join(", ", _tableColumns.Where(x => !_tableIgnoreOnUpdateColumns.Any(y => y == x)).Select(c => $"{c} = @{c}")) +
                " WHERE " + string.Join(" AND ", _tableIdentifierColumn.Select(c => $"{c} = @{c}"));

            return query;
        }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.String.</returns>
        public string GetConnectionString(string customerCode = null)
        {
            return _connectionFactory.GetConnectionString(customerCode);
        }

        #region "Single Connection/request Implementation"

        public void SetConnection(string customerCode)
        {
            _customerCode = customerCode;
            if (!string.IsNullOrEmpty(_customerCode))
            {
                _connection = new SqlConnection(_connectionFactory.GetConnectionString(customerCode));
            }
        }

        public T CreateInTransaction(T entity)
        {
            string query = GenerateCreateQuery();
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
            var result = _connection.Query<T>(query, entity, commandType: CommandType.Text);
            return result.First();
        }

        public int UpdateInTransaction(T entity)
        {
            var query = GenerateUpdateQuery();

            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
            int result = _connection.Execute(query, entity, commandType: CommandType.Text);
            return result;
        }

        public IEnumerable<T> FindInTransation(Expression<Func<T, bool>> predicatExpression)
        {
            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause}";

            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            IEnumerable<T> result = _connection.Query<T>(query, commandType: CommandType.Text);

            return result;
        }

        public T FirstOrDefaultInTransaction(Expression<Func<T, bool>> predicatExpression)
        {
            T result = null;

            var translator = new QueryTranslator();
            string whereClause = translator.Translate(predicatExpression);
            var query = $"SELECT TOP 1 {string.Join(",", _tableIdentifierColumn)}, {string.Join(",", _tableColumns)} FROM {_table} WITH (NOLOCK) WHERE {whereClause}";

            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
            var results = _connection.Query<T>(query, commandType: CommandType.Text);
            if (results != null && results.Any())
            {
                result = results.First();
            }

            return result;
        }

        #endregion

        #region "IDisposable Implementation"

        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed) return;

            if (disposing)
            {
                if (_connection.State != ConnectionState.Closed)
                {
                    // free managed resources
                    _connection.Close();
                    _connection.Dispose();
                }
            }

            isDisposed = true;
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~Repository()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        } 

        #endregion

    }

    public class udtReplicationQueueIds
    {
        public long lQueueId { get; set; }
    }
}

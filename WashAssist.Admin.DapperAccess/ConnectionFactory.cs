﻿// ***********************************************************************
// Assembly         : WashAssist.Admin.DapperDataAccess
// Author           : Sajeev SL
// Created          : 09-26-2018
//
// Last Modified By : Sajeev SL
// Last Modified On : 02-07-2020
// ***********************************************************************
// <copyright file="ConnectionFactory.cs" company="WashAssist.Admin.DapperDataAccess">
//     Copyright (c) Micrologic Associates Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using WashAssist.Admin.Core.Interfaces;
using System;
using System.Threading.Tasks;

namespace WashAssist.Admin.DapperDataAccess
{

    /// <summary>
    /// Class ConnectionFactory.
    /// </summary>
    /// <seealso cref="WashAssist.Admin.Core.Interfaces.IConnectionFactory" />
    public class ConnectionFactory : IConnectionFactory
    {
        /// <summary>
        /// The connection helper
        /// </summary>
        /// 
        private readonly IConnectionHelper _connectionHelper;
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionFactory"/> class.
        /// </summary>
        /// <param name="connectionHelper">The connection helper.</param>
        public ConnectionFactory(IConnectionHelper connectionHelper)
        {   
            _connectionHelper = connectionHelper;
            
        }

        ///// <summary>
        ///// Gets the connection.
        ///// </summary>
        ///// <param name="customerCode">The customer code.</param>
        ///// <returns>IDbConnection.</returns>
        //public IDbConnection GetConnection(string customerCode)
        //{
             
        //    var connectionstring = _connectionHelper.GetConnectingString(customerCode).Result;            
        //    if (bMaxPoolSizeOn) {
        //        connectionstring += $"Max Pool Size = {MaxConnections};";
        //    }            
        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    conn.Open();
            

            
        //    return conn;
        //}
        
        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.String.</returns>
        public string GetConnectionString(string customerCode)
        {
            return  _connectionHelper.GetConnectingString(customerCode).Result;
        }
    }
}

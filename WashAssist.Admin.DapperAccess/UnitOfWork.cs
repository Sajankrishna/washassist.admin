﻿using WashAssist.Admin.Core.Interfaces;
using System;

namespace WashAssist.Admin.DapperDataAccess
{
    public class UnitOfWork<T> : IDapperUnitOfWork<T> where T : class
    {
        private bool isDisposed;
        private readonly IRepository<T> _repository;
        private readonly IComplexTypeRepository<T> _spRepository;
        protected IConnectionFactory _connectionFactory;

        public UnitOfWork(IRepository<T> repository, IComplexTypeRepository<T> spRepository, IConnectionFactory connectionFactory)
        {
            _repository = repository;
            _spRepository = spRepository;
            _connectionFactory = connectionFactory;
        }

        public IRepository<T> GetRepository(string customerCode = null)
        {
            if(!string.IsNullOrEmpty(customerCode))
            {
                _repository.SetConnection(customerCode);
            }

            return _repository;
        }

        public IComplexTypeRepository<T> GetSpRepository()
        {
            return _spRepository;
        }

        // Dispose() calls Dispose(true)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed) return;

            if (disposing)
            {
                _repository.Dispose();
            }

            isDisposed = true;
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't
        // own unmanaged resources, but leave the other methods
        // exactly as they are.
        ~UnitOfWork()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
    }
}

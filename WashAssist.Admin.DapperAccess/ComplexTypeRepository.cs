﻿// ***********************************************************************
// Assembly         : WashAssist.Admin.DapperDataAccess
// Author           : Sajeev SL
// Created          : 06-20-2019
//
// Last Modified By : Sajeev SL
// Last Modified On : 02-12-2020
// ***********************************************************************
// <copyright file="ComplexTypeRepository.cs" company="WashAssist.Admin.DapperDataAccess">
//     Copyright (c) Micrologic Associates Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using Dapper;
using WashAssist.Admin.Core.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace WashAssist.Admin.DapperDataAccess
{
    /// <summary>
    /// Class ComplexTypeRepository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="WashAssist.Admin.Core.Interfaces.IComplexTypeRepository{T}" />
    public class ComplexTypeRepository<T> : IComplexTypeRepository<T> where T : class
    {
        /// <summary>
        /// The connection factory
        /// </summary>
        protected IConnectionFactory _connectionFactory;
        /// <summary>
        /// Initializes a new instance of the <see cref="ComplexTypeRepository{T}"/> class.
        /// </summary>
        /// <param name="connectionFactory">The connection factory.</param>
        public ComplexTypeRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        #region "Sync Execution"

        /// <summary>
        /// Runs the procedure.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        public void RunProcedure(string procName, Dictionary<string, string> parameters, string customerCode)
        {
            var dynamicParameters = GetParameters(parameters);
            if (!string.IsNullOrEmpty(procName))
            {
                using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
                {
                    conn.Open(); 
                    SqlConnection.ClearPool(conn);
                    conn.Execute(procName, dynamicParameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// Executes the procedure.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        public IEnumerable<T> ExecuteProcedure(string procName, Dictionary<string, string> parameters, string customerCode)
        {
            var dynamicParameters = GetParameters(parameters);
            if (!string.IsNullOrEmpty(procName))
            {
                using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
                {
                    conn.Open(); 
                    SqlConnection.ClearPool(conn);
                    var data = conn.Query<T>(procName, dynamicParameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                    conn.Close();
                    return data;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the paginated data.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRows">The total rows.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        public IEnumerable<T> GetPaginatedData(string procName, int pageNo, int pageSize, out long totalRows, string customerCode)
        {
            totalRows = 0;
            List<T> returnData = null;
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open(); 
                SqlConnection.ClearPool(conn);
                var parameters = new DynamicParameters();
                parameters.Add($"@CurrentPageNo", $"{pageNo}");
                parameters.Add($"@PageSize", $"{pageSize}");
                var resultSet = conn.QueryMultiple(procName, parameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                if (resultSet != null)
                {
                    totalRows = resultSet.ReadSingle<long>();
                    returnData = resultSet.Read<T>().AsList();
                }
                conn.Close();
                return returnData;
            }
        }

        /// <summary>
        /// Gets the multi result set data.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="action">The action.</param>
        /// <param name="customerCode">The customer code.</param>
        public void GetMultiResultSetData(string procName, Dictionary<string, string> parameters, Action<object> action, string customerCode)
        {
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open(); 
                SqlConnection.ClearPool(conn);
                var dynamicParameters = GetParameters(parameters);
                var multiResultSet = conn.QueryMultiple(procName, dynamicParameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                action.Invoke(multiResultSet);
                conn.Close();
            }
        }

        #endregion

        #region "Async Execution"

        /// <summary>
        /// run procedure as an asynchronous operation.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task.</returns>
        public async Task RunProcedureAsync(string procName, Dictionary<string, string> parameters, string customerCode)
        {
            var dynamicParameters = GetParameters(parameters);

            if (!string.IsNullOrEmpty(procName))
            {
                using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
                {
                    conn.Open(); 
                    SqlConnection.ClearPool(conn);
                    await conn.ExecuteAsync(procName, dynamicParameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// execute procedure as an asynchronous operation.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;IEnumerable&lt;T&gt;&gt;.</returns>
        public async Task<IEnumerable<T>> ExecuteProcedureAsync(string procName, Dictionary<string, string> parameters, string customerCode)
        {
            var dynamicParameters = GetParameters(parameters);

            if (!string.IsNullOrEmpty(procName))
            {
                using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
                {
                    conn.Open(); 
                    SqlConnection.ClearPool(conn);
                    var data = await conn.QueryAsync<T>(procName, dynamicParameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                    conn.Close();
                    return data;
                }
            }



            return null;
        }

        /// <summary>
        /// get paginated data as an asynchronous operation.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task&lt;Tuple&lt;System.Int64, IEnumerable&lt;T&gt;&gt;&gt;.</returns>
        public async Task<Tuple<long, IEnumerable<T>>> GetPaginatedDataAsync(string procName, int pageNo, int pageSize, string customerCode)
        {
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open(); 
                SqlConnection.ClearPool(conn);
                var parameters = new DynamicParameters();
                parameters.Add($"@CurrentPageNo", $"{pageNo}");
                parameters.Add($"@PageSize", $"{pageSize}");
                var resultSet = await conn.QueryMultipleAsync(procName, parameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                if (resultSet != null)
                {
                    var totalRows = await resultSet.ReadSingleAsync<long>();
                    var returnData = await resultSet.ReadAsync<T>();
                    var data = Tuple.Create(totalRows, returnData);
                    conn.Close();
                    return data;
                }
                return null;
            }
        }

        /// <summary>
        /// get multi result set data as an asynchronous operation.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="action">The action.</param>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>Task.</returns>
        public async Task GetMultiResultSetDataAsync(string procName, Dictionary<string, string> parameters, Action<object> action, string customerCode)
        {
            using (var conn = new SqlConnection(_connectionFactory.GetConnectionString(customerCode)))
            {
                conn.Open(); 
                SqlConnection.ClearPool(conn);
                var dynamicParameters = GetParameters(parameters);
                var multiResultSet = await conn.QueryMultipleAsync(procName, dynamicParameters, commandTimeout: 180, commandType: CommandType.StoredProcedure);
                action.Invoke(multiResultSet);
                conn.Close();
            }
        }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="customerCode">The customer code.</param>
        /// <returns>System.String.</returns>
        public string GetConnectionString(string customerCode)
        {
            return _connectionFactory.GetConnectionString(customerCode);
        }

        #endregion

        #region "Private Methods"

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns>DynamicParameters.</returns>
        private DynamicParameters GetParameters(Dictionary<string, string> param)
        {
            var parameters = new DynamicParameters();
            foreach (var item in param)
            {
                parameters.Add($"@{item.Key}", $"{item.Value}");
            }

            return parameters;
        }

        #endregion
    }
}

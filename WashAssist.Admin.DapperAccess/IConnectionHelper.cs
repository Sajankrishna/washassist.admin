﻿using System.Threading.Tasks;

namespace WashAssist.Admin.DapperDataAccess
{
    public interface IConnectionHelper
    {
        Task<string> GetConnectingString(string customerCode);
    }
}
